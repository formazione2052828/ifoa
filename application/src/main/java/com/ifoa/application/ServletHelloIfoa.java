package com.ifoa.application;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serial;
import com.ifoa.model.*;

public class ServletHelloIfoa extends HttpServlet {

    @Serial
    private static final long serialVersionUID = 8568010773920214577L;

    /**
     * Process the HTTP Get request
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.getWriter().print(IfoaInfo.IFOA_INFO);
        } catch(Exception e) {
            throw e;
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       doGet(request,response);
    }
}
